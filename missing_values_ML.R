install.packages('mice')
library(mice)

# distribution of missing values
md.pattern(FlowerPicks)

install.packages('randomForest')
mymice <- mice(FlowerPicks, m=10, method = 'rf')

mymice$imp$Score

mymicecomplete <- complete(mymice, 5)
summary(mymicecomplete)

# Analysis with variations of the dataset
lmft <- with(mymice, lm(Score ~ Time))
summary(pool(lmft))
